package com.example.vigiliantiapi.controllers;


import com.example.vigiliantiapi.models.EntryControl;
import com.example.vigiliantiapi.services.EntryControllerService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:8080/"})
@RestController
@RequestMapping("/ec")
@RequiredArgsConstructor
public class EntryControlController {

    private final EntryControllerService entryControllerService;

    @PostMapping("/create")
    public ResponseEntity<EntryControl> crearEntryControl(@RequestBody EntryControl entryControl) {
        EntryControl entryControlCreado = entryControllerService.crearEntryControl(entryControl);
        return ResponseEntity.ok(entryControlCreado);
    }


    @PutMapping("/edit/{id}")
    public ResponseEntity<EntryControl> editEntryControl(@PathVariable Integer id, @RequestBody EntryControl entryControl) {
        EntryControl updatedEntryControl = entryControllerService.editEntryControl(id, entryControl);

        if (updatedEntryControl != null) {
            return ResponseEntity.ok(updatedEntryControl);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/all")
    public ResponseEntity<List<EntryControl>> getAllEntryControls() {
        List<EntryControl> entryControls = entryControllerService.getAllEntryControls();
        return ResponseEntity.ok(entryControls);
    }

    @GetMapping("/{id}")
    public ResponseEntity<EntryControl> getEntryControlById(@PathVariable Integer id) {
        EntryControl entryControl = entryControllerService.getEntryControlById(id);

        if (entryControl != null) {
            return ResponseEntity.ok(entryControl);
        } else {
            return ResponseEntity.notFound().build();
        }


}

    @GetMapping("/search")
    public ResponseEntity<List<EntryControl>> searchEntryControls(@RequestParam String keyword) {
        try {
            List<EntryControl> entryControls = entryControllerService.searchEntryControls(keyword);
            return ResponseEntity.ok(entryControls);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping("/disable")
    public ResponseEntity<Void> disableEntryControls(@RequestBody List<EntryControl> entryControls) {
        try {
            entryControllerService.disableEntryControls(entryControls);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }


}
