package com.example.vigiliantiapi.controllers;


import com.example.vigiliantiapi.exceptions.ConcurrentVisitorNotFoundException;
import com.example.vigiliantiapi.models.ConcurrentVisitor;
import com.example.vigiliantiapi.services.ConcurrentVisitorService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = {"http://localhost:8080/"})
@RestController
@RequestMapping("/visitor")
@RequiredArgsConstructor
public class ConcurrentVisitorController {


    private ConcurrentVisitorService concurrentVisitorService;

    @PutMapping("/{concurrentVisitorId}")
    public ResponseEntity<String> editProfileConcurrent(@PathVariable Integer concurrentVisitorId, @RequestBody ConcurrentVisitor concurrentVisitor) {
        try {
            concurrentVisitorService. editProfileConcurrent(concurrentVisitorId, concurrentVisitor);
            return ResponseEntity.ok("Perfil del visitante concurrente actualizado correctamente.");
        } catch (ConcurrentVisitorNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Visitante concurrente no encontrado.");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Ocurrió un error al actualizar el perfil del visitante concurrente.");
        }
    }
}
