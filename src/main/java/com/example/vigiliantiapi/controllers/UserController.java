package com.example.vigiliantiapi.controllers;


import com.example.vigiliantiapi.exceptions.UnauthorizedAccessException;
import com.example.vigiliantiapi.exceptions.UserNotFoundException;
import com.example.vigiliantiapi.models.User;
import com.example.vigiliantiapi.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = {"http://localhost:8080/"})
@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @DeleteMapping("/{userId}")
    public ResponseEntity<String> deleteUser(@PathVariable Integer userId, @RequestBody User currentUser) {
        try {
            userService.deleteUserById(userId, currentUser);
            return ResponseEntity.ok("Usuario eliminado correctamente.");
        } catch (UnauthorizedAccessException e) {
            return ResponseEntity.status(403).body("Solo los usuarios de tipo ADMIN pueden eliminar usuarios.");
        } catch (UserNotFoundException e) {
            return ResponseEntity.status(404).body("Usuario no encontrado.");
        } catch (Exception e) {
            return ResponseEntity.status(500).body("Ocurrió un error al eliminar el usuario.");
        }
    }
}
