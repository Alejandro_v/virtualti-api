package com.example.vigiliantiapi.controllers;


import com.example.vigiliantiapi.services.QrGeneratorService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = {"http://localhost:8080/"})
@RestController
@RequestMapping("/qr")
public class QrGeneratorController {

    private QrGeneratorService qrGeneratorService;


    @GetMapping(value = "/generate", produces = MediaType.IMAGE_PNG_VALUE)
    public byte[] generateQRCode(@RequestParam String data, @RequestParam int width, @RequestParam int height) throws Exception {
        return qrGeneratorService.generateQRCode(data, width, height);
    }
}
