package com.example.vigiliantiapi.controllers;

import com.example.vigiliantiapi.config.JwtAuthenticationResponse;
import com.example.vigiliantiapi.config.request.SignInRequest;
import com.example.vigiliantiapi.config.request.SignUpRequest;
import com.example.vigiliantiapi.exceptions.EmailAlreadyExistsException;
import com.example.vigiliantiapi.services.AuthenticationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@CrossOrigin(origins = "http://localhost:8080/")
@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthenticationController {
    private final AuthenticationService authenticationService;



    @PostMapping("/signin")
    public ResponseEntity<JwtAuthenticationResponse> signin(@RequestBody SignInRequest request) {
        JwtAuthenticationResponse response = authenticationService.signin(request);
        return ResponseEntity.ok(response);
    }


    @PostMapping("/register_admin")
    public ResponseEntity<?> registerAdmin(@RequestBody SignUpRequest request) {
        JwtAuthenticationResponse response = authenticationService.registerAdmin(request);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/register_concierge")
    public ResponseEntity<?> registerConcierge(@RequestBody SignUpRequest request) {
        try {
            JwtAuthenticationResponse response = authenticationService.registerConcierge(request);
            return ResponseEntity.ok(response);
        } catch (EmailAlreadyExistsException e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Correo electrónico ya registrado");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error interno del servidor");
        }
    }

    @PostMapping("/register_visitor")
    public ResponseEntity<?> registerConcurrentVisitor(@RequestBody SignUpRequest request) {
        try {
            JwtAuthenticationResponse response = authenticationService.registerConcurrentVisitor(request);
            return ResponseEntity.ok(response);
        } catch (EmailAlreadyExistsException e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Correo electrónico ya registrado");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error interno del servidor");
        }
    }

}
