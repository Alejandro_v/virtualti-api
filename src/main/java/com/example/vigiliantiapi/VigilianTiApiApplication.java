package com.example.vigiliantiapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VigilianTiApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(VigilianTiApiApplication.class, args);
    }

}
