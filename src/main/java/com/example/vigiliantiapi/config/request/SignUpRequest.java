package com.example.vigiliantiapi.config.request;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SignUpRequest {

    private String email;
    private String password;
    private String name;
    private String lastName;


    //Atributos concurrentVisitor

    private String carPatent;
    private String modelVehicle;


    //Atributos concierge

    private String place;
}
