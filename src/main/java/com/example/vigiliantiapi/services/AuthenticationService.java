package com.example.vigiliantiapi.services;

import com.example.vigiliantiapi.config.JwtAuthenticationResponse;
import com.example.vigiliantiapi.config.request.SignInRequest;
import com.example.vigiliantiapi.config.request.SignUpRequest;
import com.example.vigiliantiapi.exceptions.EmailAlreadyExistsException;
import com.example.vigiliantiapi.models.Concierge;
import com.example.vigiliantiapi.models.ConcurrentVisitor;
import com.example.vigiliantiapi.models.Rol;
import com.example.vigiliantiapi.models.User;
import com.example.vigiliantiapi.repositories.ConciergeRepository;
import com.example.vigiliantiapi.repositories.ConcurrentVisitorRepository;
import com.example.vigiliantiapi.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthenticationService {

    private final UserRepository userRepository;
    private final ConciergeRepository conciergeRepository;
    private final ConcurrentVisitorRepository concurrentVisitorRepository;

    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;


    public JwtAuthenticationResponse signin(SignInRequest request) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword()));
        var user = userRepository.findByEmail(request.getEmail())
                .orElseThrow(() -> new IllegalArgumentException("Invalid email or password."));
        var jwt = jwtService.generateToken(user);
        return JwtAuthenticationResponse.builder().token(jwt).build();
    }

    public User registerUser(SignUpRequest request, String rol) throws EmailAlreadyExistsException {
        if (userRepository.existsByEmail(request.getEmail())) {
            throw new EmailAlreadyExistsException("Email is already registered");
        }

        switch (rol) {
            case "Concierge":
                var user = User.builder()
                        .email(request.getEmail())
                        .password(passwordEncoder.encode(request.getPassword()))
                        .rol(Rol.valueOf("Concierge")).build();
                return userRepository.save(user);
            case "ConcurrentVisitor":
                var user2 = User.builder()
                        .email(request.getEmail())
                        .password(passwordEncoder.encode(request.getPassword()))
                        .rol(Rol.valueOf("ConcurrentVisitor")).build();
                return userRepository.save(user2);

            default:
                var user3 = User.builder()
                        .email(request.getEmail())
                        .password(passwordEncoder.encode(request.getPassword()))
                        .rol(Rol.valueOf("Admin")).build();
                return userRepository.save(user3);

        }
    }


    public JwtAuthenticationResponse registerAdmin(SignUpRequest request){

        var user = User.builder()
                .email(request.getEmail())
                .password(passwordEncoder.encode(request.getPassword()))
                .rol(Rol.valueOf("Admin")).build();
        userRepository.save(user);

        var jwt = jwtService.generateToken(user);
        return JwtAuthenticationResponse.builder().token(jwt).build();
    }

    public JwtAuthenticationResponse registerConcierge(SignUpRequest signUpRequest) throws EmailAlreadyExistsException{
        User user = registerUser(signUpRequest, "Concierge");

        var concierge = Concierge.builder()
                .user(user)
                .conName(signUpRequest.getName())
                .conLastName(signUpRequest.getLastName())
                .conPlace(signUpRequest.getPlace())
                .build();
        conciergeRepository.save(concierge);

        var jwt = jwtService.generateToken(user);
        return JwtAuthenticationResponse.builder().token(jwt).build();
    }

    public JwtAuthenticationResponse registerConcurrentVisitor(SignUpRequest signUpRequest) throws EmailAlreadyExistsException{
        User user = registerUser(signUpRequest, "ConcurrentVisitor");

        var concurrentVisitor = ConcurrentVisitor.builder()
                .user(user)
                .visName(signUpRequest.getName())
                .visLastName(signUpRequest.getLastName())
                .visCarPatent(signUpRequest.getCarPatent())
                .visModelVehicle(signUpRequest.getModelVehicle())
                .build();
        concurrentVisitorRepository.save(concurrentVisitor);

        var jwt = jwtService.generateToken(user);
        return JwtAuthenticationResponse.builder().token(jwt).build();
    }


}
