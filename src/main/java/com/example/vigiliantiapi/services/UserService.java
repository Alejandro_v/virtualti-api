package com.example.vigiliantiapi.services;

import com.example.vigiliantiapi.exceptions.UnauthorizedAccessException;
import com.example.vigiliantiapi.exceptions.UserNotFoundException;
import com.example.vigiliantiapi.models.Rol;
import com.example.vigiliantiapi.models.User;
import com.example.vigiliantiapi.repositories.UserRepository;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


import java.nio.file.AccessDeniedException;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {
    private UserRepository userRepository;


    public UserDetailsService userDetailsService() {
        return new UserDetailsService() {
            @Override
            public UserDetails loadUserByUsername(String username) {
                return userRepository.findByEmail(username)
                        .orElseThrow(() -> new UsernameNotFoundException("User not found"));
            }
        };

    }

    public void deleteUserById(Integer userId, User currentUser) throws UnauthorizedAccessException {

        if (currentUser.getRol() != Rol.Admin) {
            throw new UnauthorizedAccessException("Solo los usuarios de tipo ADMIN pueden eliminar usuarios.");
        }

        userRepository.findById(userId).ifPresentOrElse(userRepository::delete, () -> {
            throw new UserNotFoundException("Usuario no encontrado");
        });
    }





}
