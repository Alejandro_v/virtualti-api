package com.example.vigiliantiapi.services;


import com.example.vigiliantiapi.models.EntryControl;
import com.example.vigiliantiapi.repositories.EntryControllerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class EntryControllerService {

    private final EntryControllerRepository entryControllerRepository;


    public EntryControl crearEntryControl(EntryControl entryControl) {
        return entryControllerRepository.save(entryControl);
    }

    public EntryControl editEntryControl(Integer id, EntryControl entryControl) {
        return entryControllerRepository.findById(id)
                .map(existingEntryControl -> {
                    if (entryControl.getEntry() != null) existingEntryControl.setEntry(entryControl.getEntry());
                    if (entryControl.getExit() != null) existingEntryControl.setExit(entryControl.getExit());
                    if (entryControl.getDescription() != null) existingEntryControl.setDescription(entryControl.getDescription());
                    if (entryControl.getCarPatent() != null) existingEntryControl.setCarPatent(entryControl.getCarPatent());


                    return entryControllerRepository.save(existingEntryControl);
                })
                .orElse(null);
    }

    public List<EntryControl> getAllEntryControls() {
        return entryControllerRepository.findAll();
    }

    public EntryControl getEntryControlById(Integer id) {
        return entryControllerRepository.findById(id).orElse(null);
    }


    public List<EntryControl> searchEntryControls(String keyword) {
        return entryControllerRepository.findByDescriptionContaining(keyword);
    }

    public void disableEntryControls(List<EntryControl> entryControls) {
        for (EntryControl entryControl : entryControls) {
            // Modificar el estado del ingreso para deshabilitarlo
            entryControl.setEnabled(false);
            // Guardar el cambio en la base de datos
            entryControllerRepository.save(entryControl);
        }
    }
}
