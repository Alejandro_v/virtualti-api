package com.example.vigiliantiapi.services;

import com.example.vigiliantiapi.exceptions.ConcurrentVisitorNotFoundException;
import com.example.vigiliantiapi.models.ConcurrentVisitor;
import com.example.vigiliantiapi.models.EntryControl;
import com.example.vigiliantiapi.repositories.ConcurrentVisitorRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ConcurrentVisitorService {

    private  ConcurrentVisitorRepository concurrentVisitorRepository;


    public ConcurrentVisitor createEntryConcurrentVisitor(ConcurrentVisitor concurrentVisitor) {
        return concurrentVisitorRepository.save(concurrentVisitor);
    }

    public void editProfileConcurrent(Integer concurrentVisitorId, ConcurrentVisitor concurrentVisitor) throws ConcurrentVisitorNotFoundException {
        ConcurrentVisitor existingConcurrentVisitor = concurrentVisitorRepository.findById(concurrentVisitorId)
                .orElseThrow(() -> new ConcurrentVisitorNotFoundException("Visitante concurrente no encontrado"));

        // Actualizar los atributos del visitante concurrente con los nuevos valores proporcionados
        if (concurrentVisitor.getVisName() != null) {
            existingConcurrentVisitor.setVisName(concurrentVisitor.getVisName());
        }
        if (concurrentVisitor.getVisLastName() != null) {
            existingConcurrentVisitor.setVisLastName(concurrentVisitor.getVisLastName());
        }
        if (concurrentVisitor.getVisCarPatent() != null) {
            existingConcurrentVisitor.setVisCarPatent(concurrentVisitor.getVisCarPatent());
        }
        if (concurrentVisitor.getVisModelVehicle() != null) {
            existingConcurrentVisitor.setVisModelVehicle(concurrentVisitor.getVisModelVehicle());
        }

        // Guardar el visitante concurrente actualizado
        concurrentVisitorRepository.save(existingConcurrentVisitor);
    }
}
