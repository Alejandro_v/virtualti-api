package com.example.vigiliantiapi.repositories;

import com.example.vigiliantiapi.models.Concierge;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface ConciergeRepository extends JpaRepository<Concierge, Integer> {

}
