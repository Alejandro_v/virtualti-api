package com.example.vigiliantiapi.repositories;

import com.example.vigiliantiapi.models.EntryControl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EntryControllerRepository extends JpaRepository<EntryControl,Integer> {
    List<EntryControl> findByDescriptionContaining(String keyword);
}
