package com.example.vigiliantiapi.repositories;

import com.example.vigiliantiapi.models.ConcurrentVisitor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConcurrentVisitorRepository extends JpaRepository<ConcurrentVisitor, Integer> {
}
