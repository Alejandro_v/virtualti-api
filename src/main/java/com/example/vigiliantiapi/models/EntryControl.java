package com.example.vigiliantiapi.models;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "entryControl")
public class EntryControl {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idEntryControl")
    private Integer idEntryControl;

    @Column(name = "ecName")
    private String ecName;

    @Column(name = "ecLastName")
    private String ecLastName;

    @Column(name = "ecEntry")
    private String entry;
    @Column(name = "ecExit")
    private String exit;
    @Column(name = "ecDescription")
    private String description;
    @Column(name = "ecCarPatent",nullable = true)
    private String carPatent;

    public void setEnabled(boolean b) {
    }
}
