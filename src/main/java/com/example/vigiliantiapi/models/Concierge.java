package com.example.vigiliantiapi.models;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "concierge")
public class Concierge  {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idConcierge")
    private Integer idConcierge;


    @Column(name = "conName")
    private String conName;

    @Column(name = "conLastName")
    private String conLastName;

    @Column(name="conPlace")
    private String conPlace;


    @MapsId
    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

}
