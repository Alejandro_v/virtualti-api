package com.example.vigiliantiapi.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "concurrentVisitor")

public class ConcurrentVisitor {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idConcurrentVisitor")
    private Integer idConcierge;


    @Column(name = "visName")
    private String visName;

    @Column(name = "visLastName")
    private String visLastName;

    @Column(name = "visCarPatent")
    private String visCarPatent;

    @Column(name = "visModelVehicle")
    private String visModelVehicle;



    @MapsId
    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

}
